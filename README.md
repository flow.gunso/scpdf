# scPDF: scans to PDF

This tools helps you generate PDFs from images.

![An image speaks louder than words](docs/essentially.jpg)

Provide a path to this tool:
- A multiple page PDF will output using all images from any folder found within.
- A single page PDF will output from any images found within.
