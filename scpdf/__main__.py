#!/usr/bin/env python

import fpdf
import argparse
from pathlib import Path
from PIL import Image
import os, shutil

# Setup the argument parser.
parser = argparse.ArgumentParser(description = """This tools helps you generate PDFs from images.
- A multiple page PDF will output using all images from any folder found within.
- A single page PDF will output from any images found within.""",
formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('path', type=str, help='path to root folder')
parser.add_argument('--force-delete', action='store_true',  help="this option prevent the scans deletion")
args = parser.parse_args()

# Init the given path.
root = Path(args.path)

# Check if given path is actually a path.
if not root.is_dir():
    parser.print_help()
    exit(1)

# Loop over the given path.
for path in root.iterdir():

    # If it's a path, iterate over the file within, generate a pdf from ordered image file.
    if path.is_dir():
        # Pre-loop the path to find the largest (width, height) image.
        width=0
        height=0
        for file in sorted(path.iterdir()):
            image = Image.open(file.as_posix())
            if image.size[0]>width:
                width=image.size[0]
            if image.size[1]>height:
                height=image.size[1]

        # Init the pdf.
        pdf = fpdf.FPDF(format=(width, height))
        pdf.set_margins(0, 0, 0)
        pdf.name = path.name + ".pdf"

        # Add the pages.
        for file in sorted(path.iterdir()):
            image = Image.open(file.as_posix())
            pdf.add_page()
            pdf.image(file.as_posix(), 0, 0, image.size[0], image.size[1])
        # Unload the image.
        image = None

        # Output the pdf.
        pdf.output(root.joinpath(pdf.name))

        # Keep source if not stated otherwise.
        if args.force_delete:
            shutil.rmtree(path)

    # Else, a file, generate the pdf.
    else:
        # Init the pdf.
        image = Image.open(path.as_posix())
        pdf = fpdf.FPDF(format=image.size)
        pdf.set_margins(0, 0, 0)

        # Add the single page.
        pdf.add_page()
        pdf.image(path.as_posix(), 0, 0, image.size[0], image.size[1])
        pdf.name = path.stem + ".pdf"

        # Unload the image.
        image = None

        # Output the pdf.
        pdf.output(root.joinpath(pdf.name))

        # Keep source if not stated otherwise.
        if args.force_delete:
            os.remove(path)
